#!/usr/bin/env bash
#
#
# Copyright (C) Yash-Garg <ben10.yashgarg@gmail.com>
# SPDX-License-Identifier: GPL-v3.0-only
#

# Colors for script
GRN="\033[01;32m"
RED="\033[01;31m"
RST="\033[0m"
YLW="\033[01;33m"

# Alias for echo to handle escape codes like colors
function echo() {
    command echo -e "$@"
}

# Prints a formatted header; used for outlining
function echoText() {

    echo -e "${RED}"
    echo -e "====$( for i in $(seq ${#1}); do echo -e "=\c"; done )===="
    echo -e "==  ${1}  =="
    echo -e "====$( for i in $(seq ${#1}); do echo -e "=\c"; done )===="
    echo -e "${RST}"
}

# A function that installs git and shows a success output msg
function start() {
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install git gnupg
}

# Executing functions below
start;

echoText "Checking GIT and GPG version"

git --version
gpg --version

echoText "Everything installed correctly!"
